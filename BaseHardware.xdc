#Sch name = JA1
set_property PACKAGE_PIN J1 [get_ports {JA0}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {JA0}]

#Sch name = JA2
set_property PACKAGE_PIN L2 [get_ports {JA1}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {JA1}]

#Sch name = JC1
set_property PACKAGE_PIN K17 [get_ports {JC0}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {JC0}]
